# mysql2pgsql-docker

Maria to PostgreSQL with pgloader and Docker

## Setup

0. Modfier le maria de base

```sql
alter table clients_areas drop column check_curl;
alter table clients_areas drop column lead_curl;
alter table lead drop column buying_price;

drop table knex_migrations;
drop table knex_migrations_lock;
````

1. Place your Maria dump inside migrations folder and Postgres dump in dump folder

2. Run `docker-compose up -d --build`

3. Add a role exploit in postgres `docker-compose exec postgres psql -U postgres -d postgres -c 'create user exploit'`

4. Load new psql db in postgres `cat dump/pg-dump.sql | docker exec -i postgres-db psql -U postgres postgres`

5. When containes are online run `docker-compose exec postgres pgloader pgload_maria_to_postgres.load`

6. When the db is loaded run `docker-compose exec postgres pg_dump -U postgres > dump/pg-dump-charged.sql`

7. Import the db into postgres `cat dump/pg-dump-charged.sql | docker exec -i postgres-database psql -U exploit join_venture`

> doc to cast type
<https://pgloader.readthedocs.io/en/latest/ref/mysql.html#default-mysql-casting-rules>

### TODO

- Maria Migration
- Cast timestamp
- echo >> "ALTER TABLE la_table
  DROP COLUMN la_colonne;" le_fichier_sql_de_dump.sql
