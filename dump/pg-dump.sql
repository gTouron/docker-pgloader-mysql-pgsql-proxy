--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: invoice_status_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.invoice_status_enum AS ENUM (
    'PAID',
    'DECLINED',
    'WAITING',
    'CANCELED'
);


ALTER TYPE public.invoice_status_enum OWNER TO exploit;

--
-- Name: invoice_type_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.invoice_type_enum AS ENUM (
    'BILL',
    'RECEIPT'
);


ALTER TYPE public.invoice_type_enum OWNER TO exploit;

--
-- Name: lead_status_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.lead_status_enum AS ENUM (
    'NEW',
    'DISPUTED',
    'READY_FOR_SALE',
    'WAITING_DEMAND',
    'WAITING_FOR_QUALIFICATION',
    'QUALIFIED',
    'SOLD_MUTU',
    'SOLD_EXCLU',
    'UNREAD',
    'TEST_REJECTED',
    'ANONYMISED',
    'OUT_OF_TARGET',
    'OP_REFUSED'
);


ALTER TYPE public.lead_status_enum OWNER TO exploit;

--
-- Name: payment_type_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.payment_type_enum AS ENUM (
    'PREPAID',
    'CONTINUOUS_AUTO',
    'CONTINUOUS_MANUAL',
    'PROVIDER_PAYOUT'
);


ALTER TYPE public.payment_type_enum OWNER TO exploit;

--
-- Name: role_name_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.role_name_enum AS ENUM (
    'SUPER',
    'ADMIN',
    'OPERATOR',
    'CLIENT',
    'PROVIDER'
);


ALTER TYPE public.role_name_enum OWNER TO exploit;

--
-- Name: transaction_type_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.transaction_type_enum AS ENUM (
    'BILL',
    'LEAD',
    'CREDIT',
    'PREPAID'
);


ALTER TYPE public.transaction_type_enum OWNER TO exploit;

--
-- Name: webhook_type_enum; Type: TYPE; Schema: public; Owner: exploit
--

CREATE TYPE public.webhook_type_enum AS ENUM (
    'GET',
    'POST',
    'PUT',
    'DELETE'
);


ALTER TYPE public.webhook_type_enum OWNER TO exploit;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: address; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.address (
    id integer NOT NULL,
    line1 character varying NOT NULL,
    line2 character varying,
    state character varying,
    country character varying(2) NOT NULL,
    city character varying NOT NULL,
    postal_code character varying,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.address OWNER TO exploit;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO exploit;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;


--
-- Name: area; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.area (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    display_name character varying(64) NOT NULL,
    default_price real NOT NULL,
    default_share_price real NOT NULL,
    default_ref_share real NOT NULL,
    default_cpl real NOT NULL,
    default_should_lead_qualified boolean DEFAULT false NOT NULL,
    uniqueness_duration integer NOT NULL,
    pool_life_time integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.area OWNER TO exploit;

--
-- Name: area_default_condition; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.area_default_condition (
    id integer NOT NULL,
    value character varying(100) NOT NULL,
    operator character varying(10) NOT NULL,
    area_key_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.area_default_condition OWNER TO exploit;

--
-- Name: area_default_condition_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.area_default_condition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_default_condition_id_seq OWNER TO exploit;

--
-- Name: area_default_condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.area_default_condition_id_seq OWNED BY public.area_default_condition.id;


--
-- Name: area_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_id_seq OWNER TO exploit;

--
-- Name: area_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.area_id_seq OWNED BY public.area.id;


--
-- Name: area_key; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.area_key (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    display_name character varying,
    optional boolean DEFAULT false NOT NULL,
    computed text,
    area_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.area_key OWNER TO exploit;

--
-- Name: area_key_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.area_key_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_key_id_seq OWNER TO exploit;

--
-- Name: area_key_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.area_key_id_seq OWNED BY public.area_key.id;


--
-- Name: client; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.client (
    id integer NOT NULL,
    weight real DEFAULT 1 NOT NULL,
    company_id integer NOT NULL,
    webhook_id integer,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.client OWNER TO exploit;

--
-- Name: client_default_condition; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.client_default_condition (
    id integer NOT NULL,
    value character varying(100) NOT NULL,
    operator character varying(10) NOT NULL,
    client_id integer NOT NULL,
    area_key_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.client_default_condition OWNER TO exploit;

--
-- Name: client_default_condition_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.client_default_condition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_default_condition_id_seq OWNER TO exploit;

--
-- Name: client_default_condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.client_default_condition_id_seq OWNED BY public.client_default_condition.id;


--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_id_seq OWNER TO exploit;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.client_id_seq OWNED BY public.client.id;


--
-- Name: clients_areas; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.clients_areas (
    id integer NOT NULL,
    tp_lead_sharing_limit integer,
    tp_price integer NOT NULL,
    tp_share_price integer NOT NULL,
    tp_should_lead_qualified boolean DEFAULT false NOT NULL,
    client_id integer NOT NULL,
    area_id integer NOT NULL,
    webhook_id integer,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.clients_areas OWNER TO exploit;

--
-- Name: clients_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.clients_areas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_areas_id_seq OWNER TO exploit;

--
-- Name: clients_areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.clients_areas_id_seq OWNED BY public.clients_areas.id;


--
-- Name: company; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.company (
    id integer NOT NULL,
    name character varying NOT NULL,
    display_name character varying,
    registration_number character varying,
    vat character varying,
    address_id integer,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.company OWNER TO exploit;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO exploit;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.invoice (
    id integer NOT NULL,
    type public.invoice_type_enum NOT NULL,
    status public.invoice_status_enum NOT NULL,
    stripe_invoice character varying,
    payment_id uuid NOT NULL,
    transaction_id uuid,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.invoice OWNER TO exploit;

--
-- Name: invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.invoice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invoice_id_seq OWNER TO exploit;

--
-- Name: invoice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.invoice_id_seq OWNED BY public.invoice.id;


--
-- Name: lead; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.lead (
    id integer NOT NULL,
    first_name character varying(45) NOT NULL,
    last_name character varying(45) NOT NULL,
    phone character varying(40) NOT NULL,
    email character varying NOT NULL,
    zip_code character varying(45) NOT NULL,
    aff_sub text,
    refers_to integer,
    status public.lead_status_enum NOT NULL,
    reason character varying,
    rating integer NOT NULL,
    is_exported boolean DEFAULT false NOT NULL,
    zip_id integer NOT NULL,
    sitekey_id integer NOT NULL,
    area_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.lead OWNER TO exploit;

--
-- Name: lead_external_ref; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.lead_external_ref (
    id integer NOT NULL,
    client_name character varying NOT NULL,
    external_ref character varying NOT NULL,
    lead_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.lead_external_ref OWNER TO exploit;

--
-- Name: lead_external_ref_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.lead_external_ref_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lead_external_ref_id_seq OWNER TO exploit;

--
-- Name: lead_external_ref_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.lead_external_ref_id_seq OWNED BY public.lead_external_ref.id;


--
-- Name: lead_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.lead_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lead_id_seq OWNER TO exploit;

--
-- Name: lead_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.lead_id_seq OWNED BY public.lead.id;


--
-- Name: lead_rejected; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.lead_rejected (
    id integer NOT NULL,
    email character varying,
    phone character varying,
    message text,
    type character varying,
    lead_payload text,
    error_payload text,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.lead_rejected OWNER TO exploit;

--
-- Name: lead_rejected_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.lead_rejected_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lead_rejected_id_seq OWNER TO exploit;

--
-- Name: lead_rejected_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.lead_rejected_id_seq OWNED BY public.lead_rejected.id;


--
-- Name: leads_properties; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.leads_properties (
    id integer NOT NULL,
    value character varying(100) NOT NULL,
    lead_id integer NOT NULL,
    area_key_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.leads_properties OWNER TO exploit;

--
-- Name: leads_properties_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.leads_properties_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.leads_properties_id_seq OWNER TO exploit;

--
-- Name: leads_properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.leads_properties_id_seq OWNED BY public.leads_properties.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.migrations OWNER TO exploit;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO exploit;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.notification (
    id integer NOT NULL,
    sms boolean DEFAULT false NOT NULL,
    company_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.notification OWNER TO exploit;

--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_id_seq OWNER TO exploit;

--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- Name: order; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public."order" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name character varying(100) NOT NULL,
    capping integer NOT NULL,
    price real NOT NULL,
    start_at timestamp without time zone NOT NULL,
    end_at timestamp without time zone,
    daily_budget bigint NOT NULL,
    capping_max integer NOT NULL,
    share_limit integer NOT NULL,
    status character varying NOT NULL,
    client_area_id integer NOT NULL,
    webhook_id integer,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public."order" OWNER TO exploit;

--
-- Name: order_condition; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.order_condition (
    id integer NOT NULL,
    value character varying(100) NOT NULL,
    operator character varying(10) NOT NULL,
    order_id uuid NOT NULL,
    area_key_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.order_condition OWNER TO exploit;

--
-- Name: order_condition_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.order_condition_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_condition_id_seq OWNER TO exploit;

--
-- Name: order_condition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.order_condition_id_seq OWNED BY public.order_condition.id;


--
-- Name: orders_leads; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.orders_leads (
    id integer NOT NULL,
    status character varying NOT NULL,
    selling_price real NOT NULL,
    order_id uuid NOT NULL,
    lead_id integer NOT NULL,
    invoice_id integer,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.orders_leads OWNER TO exploit;

--
-- Name: orders_leads_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.orders_leads_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_leads_id_seq OWNER TO exploit;

--
-- Name: orders_leads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.orders_leads_id_seq OWNED BY public.orders_leads.id;


--
-- Name: orders_zips; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.orders_zips (
    id integer NOT NULL,
    order_id uuid NOT NULL,
    zip_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.orders_zips OWNER TO exploit;

--
-- Name: orders_zips_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.orders_zips_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_zips_id_seq OWNER TO exploit;

--
-- Name: orders_zips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.orders_zips_id_seq OWNED BY public.orders_zips.id;


--
-- Name: payment; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.payment (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    balance real NOT NULL,
    type public.payment_type_enum DEFAULT 'CONTINUOUS_MANUAL'::public.payment_type_enum NOT NULL,
    stripe_customer character varying(30),
    invoice_threshold real,
    company_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.payment OWNER TO exploit;

--
-- Name: provider; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.provider (
    id integer NOT NULL,
    company_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.provider OWNER TO exploit;

--
-- Name: provider_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provider_id_seq OWNER TO exploit;

--
-- Name: provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.provider_id_seq OWNED BY public.provider.id;


--
-- Name: providers_areas; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.providers_areas (
    id integer NOT NULL,
    cpl real,
    ref_share real,
    is_cpl boolean DEFAULT false NOT NULL,
    provider_id integer NOT NULL,
    area_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.providers_areas OWNER TO exploit;

--
-- Name: providers_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.providers_areas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.providers_areas_id_seq OWNER TO exploit;

--
-- Name: providers_areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.providers_areas_id_seq OWNED BY public.providers_areas.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.role (
    id integer NOT NULL,
    name public.role_name_enum NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.role OWNER TO exploit;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO exploit;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: sitekey; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.sitekey (
    id integer NOT NULL,
    name character varying NOT NULL,
    provider_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.sitekey OWNER TO exploit;

--
-- Name: sitekey_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.sitekey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sitekey_id_seq OWNER TO exploit;

--
-- Name: sitekey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.sitekey_id_seq OWNED BY public.sitekey.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.transaction (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    balance real NOT NULL,
    amount real NOT NULL,
    type public.transaction_type_enum NOT NULL,
    reason character varying,
    lead_id integer,
    payment_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.transaction OWNER TO exploit;

--
-- Name: user; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    first_name character varying(64),
    last_name character varying(64),
    phone character varying(64),
    refresh_token character varying,
    company_creator boolean DEFAULT false NOT NULL,
    is_disabled boolean DEFAULT false NOT NULL,
    company_id integer,
    role_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public."user" OWNER TO exploit;

--
-- Name: webhook; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.webhook (
    id integer NOT NULL,
    type public.webhook_type_enum NOT NULL,
    url text NOT NULL,
    payload text,
    company_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.webhook OWNER TO exploit;

--
-- Name: webhook_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.webhook_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.webhook_id_seq OWNER TO exploit;

--
-- Name: webhook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.webhook_id_seq OWNED BY public.webhook.id;


--
-- Name: ws_logs; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.ws_logs (
    id integer NOT NULL,
    client_name character varying NOT NULL,
    status character varying NOT NULL,
    type character varying NOT NULL,
    lead_payload text,
    client_payload text,
    client_response text,
    lead_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.ws_logs OWNER TO exploit;

--
-- Name: ws_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.ws_logs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ws_logs_id_seq OWNER TO exploit;

--
-- Name: ws_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.ws_logs_id_seq OWNED BY public.ws_logs.id;


--
-- Name: zip; Type: TABLE; Schema: public; Owner: exploit
--

CREATE TABLE public.zip (
    id integer NOT NULL,
    code character varying(45),
    name character varying(45),
    region character varying,
    created_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT (now())::timestamp without time zone NOT NULL
);


ALTER TABLE public.zip OWNER TO exploit;

--
-- Name: zip_id_seq; Type: SEQUENCE; Schema: public; Owner: exploit
--

CREATE SEQUENCE public.zip_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zip_id_seq OWNER TO exploit;

--
-- Name: zip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: exploit
--

ALTER SEQUENCE public.zip_id_seq OWNED BY public.zip.id;


--
-- Name: address id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);


--
-- Name: area id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area ALTER COLUMN id SET DEFAULT nextval('public.area_id_seq'::regclass);


--
-- Name: area_default_condition id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_default_condition ALTER COLUMN id SET DEFAULT nextval('public.area_default_condition_id_seq'::regclass);


--
-- Name: area_key id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_key ALTER COLUMN id SET DEFAULT nextval('public.area_key_id_seq'::regclass);


--
-- Name: client id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);


--
-- Name: client_default_condition id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client_default_condition ALTER COLUMN id SET DEFAULT nextval('public.client_default_condition_id_seq'::regclass);


--
-- Name: clients_areas id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.clients_areas ALTER COLUMN id SET DEFAULT nextval('public.clients_areas_id_seq'::regclass);


--
-- Name: company id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);


--
-- Name: invoice id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.invoice ALTER COLUMN id SET DEFAULT nextval('public.invoice_id_seq'::regclass);


--
-- Name: lead id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead ALTER COLUMN id SET DEFAULT nextval('public.lead_id_seq'::regclass);


--
-- Name: lead_external_ref id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead_external_ref ALTER COLUMN id SET DEFAULT nextval('public.lead_external_ref_id_seq'::regclass);


--
-- Name: lead_rejected id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead_rejected ALTER COLUMN id SET DEFAULT nextval('public.lead_rejected_id_seq'::regclass);


--
-- Name: leads_properties id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.leads_properties ALTER COLUMN id SET DEFAULT nextval('public.leads_properties_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- Name: order_condition id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.order_condition ALTER COLUMN id SET DEFAULT nextval('public.order_condition_id_seq'::regclass);


--
-- Name: orders_leads id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_leads ALTER COLUMN id SET DEFAULT nextval('public.orders_leads_id_seq'::regclass);


--
-- Name: orders_zips id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_zips ALTER COLUMN id SET DEFAULT nextval('public.orders_zips_id_seq'::regclass);


--
-- Name: provider id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.provider ALTER COLUMN id SET DEFAULT nextval('public.provider_id_seq'::regclass);


--
-- Name: providers_areas id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.providers_areas ALTER COLUMN id SET DEFAULT nextval('public.providers_areas_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: sitekey id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.sitekey ALTER COLUMN id SET DEFAULT nextval('public.sitekey_id_seq'::regclass);


--
-- Name: webhook id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.webhook ALTER COLUMN id SET DEFAULT nextval('public.webhook_id_seq'::regclass);


--
-- Name: ws_logs id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.ws_logs ALTER COLUMN id SET DEFAULT nextval('public.ws_logs_id_seq'::regclass);


--
-- Name: zip id; Type: DEFAULT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.zip ALTER COLUMN id SET DEFAULT nextval('public.zip_id_seq'::regclass);


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.address (id, line1, line2, state, country, city, postal_code, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: area; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.area (id, name, display_name, default_price, default_share_price, default_ref_share, default_cpl, default_should_lead_qualified, uniqueness_duration, pool_life_time, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: area_default_condition; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.area_default_condition (id, value, operator, area_key_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: area_key; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.area_key (id, name, display_name, optional, computed, area_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.client (id, weight, company_id, webhook_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: client_default_condition; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.client_default_condition (id, value, operator, client_id, area_key_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: clients_areas; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.clients_areas (id, tp_lead_sharing_limit, tp_price, tp_share_price, tp_should_lead_qualified, client_id, area_id, webhook_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.company (id, name, display_name, registration_number, vat, address_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: invoice; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.invoice (id, type, status, stripe_invoice, payment_id, transaction_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: lead; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.lead (id, first_name, last_name, phone, email, zip_code, aff_sub, refers_to, status, reason, rating, is_exported, zip_id, sitekey_id, area_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: lead_external_ref; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.lead_external_ref (id, client_name, external_ref, lead_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: lead_rejected; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.lead_rejected (id, email, phone, message, type, lead_payload, error_payload, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: leads_properties; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.leads_properties (id, value, lead_id, area_key_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.migrations (id, "timestamp", name) FROM stdin;
1	1607011129995	addRole1607011129995
2	1607012830010	addAddress1607012830010
3	1607013230323	addCompany1607013230323
4	1607014585084	addUser1607014585084
5	1607015591735	addArea1607015591735
6	1607016294818	addProvider1607016294818
7	1607016535870	addWebhook1607016535870
8	1607016535889	addClient1607016535889
9	1607016904577	addSitekey1607016904577
10	1607032856420	addProvidersAreas1607032856420
11	1607033847364	addClientsAreas1607033847364
12	1607034238492	addZip1607034238492
13	1607034608850	addLead1607034608850
14	1607035251604	addOrder1607035251604
15	1607035763171	addPayment1607035763171
16	1607036318198	addTransaction1607036318198
17	1607036677350	addInvoice1607036677350
18	1607037004584	addOrdersLeads1607037004584
19	1607037423919	addOrdersZips1607037423919
20	1607037736103	addAreaKey1607037736103
21	1607038039843	addClientDefaultCondition1607038039843
22	1607038275473	addAreaDefaultCondition1607038275473
23	1607038456683	addOrderCondition1607038456683
24	1607038572351	addLeadsProperties1607038572351
25	1607038875398	addNotification1607038875398
26	1607039010471	addWsLogs1607039010471
27	1607039010480	addLeadRejected1607039010480
28	1607039010490	addLeadExternalRef1607039010490
\.


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.notification (id, sms, company_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public."order" (id, name, capping, price, start_at, end_at, daily_budget, capping_max, share_limit, status, client_area_id, webhook_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: order_condition; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.order_condition (id, value, operator, order_id, area_key_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: orders_leads; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.orders_leads (id, status, selling_price, order_id, lead_id, invoice_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: orders_zips; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.orders_zips (id, order_id, zip_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: payment; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.payment (id, balance, type, stripe_customer, invoice_threshold, company_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: provider; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.provider (id, company_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: providers_areas; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.providers_areas (id, cpl, ref_share, is_cpl, provider_id, area_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.role (id, name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: sitekey; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.sitekey (id, name, provider_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.transaction (id, balance, amount, type, reason, lead_id, payment_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public."user" (id, email, password, first_name, last_name, phone, refresh_token, company_creator, is_disabled, company_id, role_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: webhook; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.webhook (id, type, url, payload, company_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: ws_logs; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.ws_logs (id, client_name, status, type, lead_payload, client_payload, client_response, lead_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: zip; Type: TABLE DATA; Schema: public; Owner: exploit
--

COPY public.zip (id, code, name, region, created_at, updated_at) FROM stdin;
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.address_id_seq', 1, false);


--
-- Name: area_default_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.area_default_condition_id_seq', 1, false);


--
-- Name: area_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.area_id_seq', 1, false);


--
-- Name: area_key_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.area_key_id_seq', 1, false);


--
-- Name: client_default_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.client_default_condition_id_seq', 1, false);


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.client_id_seq', 1, false);


--
-- Name: clients_areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.clients_areas_id_seq', 1, false);


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.company_id_seq', 1, false);


--
-- Name: invoice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.invoice_id_seq', 1, false);


--
-- Name: lead_external_ref_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.lead_external_ref_id_seq', 1, false);


--
-- Name: lead_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.lead_id_seq', 1, false);


--
-- Name: lead_rejected_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.lead_rejected_id_seq', 1, false);


--
-- Name: leads_properties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.leads_properties_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.migrations_id_seq', 28, true);


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.notification_id_seq', 1, false);


--
-- Name: order_condition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.order_condition_id_seq', 1, false);


--
-- Name: orders_leads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.orders_leads_id_seq', 1, false);


--
-- Name: orders_zips_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.orders_zips_id_seq', 1, false);


--
-- Name: provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.provider_id_seq', 1, false);


--
-- Name: providers_areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.providers_areas_id_seq', 1, false);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.role_id_seq', 1, false);


--
-- Name: sitekey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.sitekey_id_seq', 1, false);


--
-- Name: webhook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.webhook_id_seq', 1, false);


--
-- Name: ws_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.ws_logs_id_seq', 1, false);


--
-- Name: zip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: exploit
--

SELECT pg_catalog.setval('public.zip_id_seq', 1, false);


--
-- Name: company PK_056f7854a7afdba7cbd6d45fc20; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT "PK_056f7854a7afdba7cbd6d45fc20" PRIMARY KEY (id);


--
-- Name: order PK_1031171c13130102495201e3e20; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY (id);


--
-- Name: zip PK_152e2076466bd9e5f354ab50aca; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.zip
    ADD CONSTRAINT "PK_152e2076466bd9e5f354ab50aca" PRIMARY KEY (id);


--
-- Name: lead_rejected PK_15cc8364e0a60318a94ccebe371; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead_rejected
    ADD CONSTRAINT "PK_15cc8364e0a60318a94ccebe371" PRIMARY KEY (id);


--
-- Name: invoice PK_15d25c200d9bcd8a33f698daf18; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT "PK_15d25c200d9bcd8a33f698daf18" PRIMARY KEY (id);


--
-- Name: client_default_condition PK_1abfea516a1cfe1198347e87a3b; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client_default_condition
    ADD CONSTRAINT "PK_1abfea516a1cfe1198347e87a3b" PRIMARY KEY (id);


--
-- Name: area PK_39d5e4de490139d6535d75f42ff; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area
    ADD CONSTRAINT "PK_39d5e4de490139d6535d75f42ff" PRIMARY KEY (id);


--
-- Name: providers_areas PK_47cbba7c83ec0f9c91263dc33c8; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.providers_areas
    ADD CONSTRAINT "PK_47cbba7c83ec0f9c91263dc33c8" PRIMARY KEY (id);


--
-- Name: clients_areas PK_529c27e83908739fbcd27ee2aee; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.clients_areas
    ADD CONSTRAINT "PK_529c27e83908739fbcd27ee2aee" PRIMARY KEY (id);


--
-- Name: provider PK_6ab2f66d8987bf1bfdd6136a2d5; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.provider
    ADD CONSTRAINT "PK_6ab2f66d8987bf1bfdd6136a2d5" PRIMARY KEY (id);


--
-- Name: notification PK_705b6c7cdf9b2c2ff7ac7872cb7; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "PK_705b6c7cdf9b2c2ff7ac7872cb7" PRIMARY KEY (id);


--
-- Name: area_default_condition PK_729d540a21a3c27853843d6c4e3; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_default_condition
    ADD CONSTRAINT "PK_729d540a21a3c27853843d6c4e3" PRIMARY KEY (id);


--
-- Name: lead_external_ref PK_79eacef4b0d4caff6744d5782c0; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead_external_ref
    ADD CONSTRAINT "PK_79eacef4b0d4caff6744d5782c0" PRIMARY KEY (id);


--
-- Name: transaction PK_89eadb93a89810556e1cbcd6ab9; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9" PRIMARY KEY (id);


--
-- Name: migrations PK_8c82d7f526340ab734260ea46be; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT "PK_8c82d7f526340ab734260ea46be" PRIMARY KEY (id);


--
-- Name: orders_leads PK_957d29b5d4e6dd6b4d7748f5c34; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_leads
    ADD CONSTRAINT "PK_957d29b5d4e6dd6b4d7748f5c34" PRIMARY KEY (id);


--
-- Name: client PK_96da49381769303a6515a8785c7; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT "PK_96da49381769303a6515a8785c7" PRIMARY KEY (id);


--
-- Name: area_key PK_9b35975ba12db22940292817120; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_key
    ADD CONSTRAINT "PK_9b35975ba12db22940292817120" PRIMARY KEY (id);


--
-- Name: ws_logs PK_9e2cbfc084f16aee104aa24b456; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.ws_logs
    ADD CONSTRAINT "PK_9e2cbfc084f16aee104aa24b456" PRIMARY KEY (id);


--
-- Name: role PK_b36bcfe02fc8de3c57a8b2391c2; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT "PK_b36bcfe02fc8de3c57a8b2391c2" PRIMARY KEY (id);


--
-- Name: leads_properties PK_c63a2f24154347b3a7348c464aa; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.leads_properties
    ADD CONSTRAINT "PK_c63a2f24154347b3a7348c464aa" PRIMARY KEY (id);


--
-- Name: lead PK_ca96c1888f7dcfccab72b72fffa; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead
    ADD CONSTRAINT "PK_ca96c1888f7dcfccab72b72fffa" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: order_condition PK_d222f8594a1ab9cd18a029d4cfb; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.order_condition
    ADD CONSTRAINT "PK_d222f8594a1ab9cd18a029d4cfb" PRIMARY KEY (id);


--
-- Name: address PK_d92de1f82754668b5f5f5dd4fd5; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT "PK_d92de1f82754668b5f5f5dd4fd5" PRIMARY KEY (id);


--
-- Name: webhook PK_e6765510c2d078db49632b59020; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.webhook
    ADD CONSTRAINT "PK_e6765510c2d078db49632b59020" PRIMARY KEY (id);


--
-- Name: orders_zips PK_e70d94123808ff19bdfac86f00a; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_zips
    ADD CONSTRAINT "PK_e70d94123808ff19bdfac86f00a" PRIMARY KEY (id);


--
-- Name: sitekey PK_ebcd40efa9949bb2390f5e0fe4c; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.sitekey
    ADD CONSTRAINT "PK_ebcd40efa9949bb2390f5e0fe4c" PRIMARY KEY (id);


--
-- Name: payment PK_fcaec7df5adf9cac408c686b2ab; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT "PK_fcaec7df5adf9cac408c686b2ab" PRIMARY KEY (id);


--
-- Name: sitekey UQ_543c8c07a0ed190d8fb1ce6168b; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.sitekey
    ADD CONSTRAINT "UQ_543c8c07a0ed190d8fb1ce6168b" UNIQUE (name);


--
-- Name: company UQ_a76c5cd486f7779bd9c319afd27; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT "UQ_a76c5cd486f7779bd9c319afd27" UNIQUE (name);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: clients_areas FK_07133dc3714af81f36199a8da76; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.clients_areas
    ADD CONSTRAINT "FK_07133dc3714af81f36199a8da76" FOREIGN KEY (webhook_id) REFERENCES public.webhook(id);


--
-- Name: lead_external_ref FK_095c965771b5f8a247e0ad9d5f5; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead_external_ref
    ADD CONSTRAINT "FK_095c965771b5f8a247e0ad9d5f5" FOREIGN KEY (lead_id) REFERENCES public.lead(id);


--
-- Name: company FK_1333bb935c62afe403dd22e5372; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT "FK_1333bb935c62afe403dd22e5372" FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- Name: orders_zips FK_2457bbef40fbf43bde87a184a14; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_zips
    ADD CONSTRAINT "FK_2457bbef40fbf43bde87a184a14" FOREIGN KEY (zip_id) REFERENCES public.zip(id);


--
-- Name: client_default_condition FK_258c8610b040dbc83d70c78d45d; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client_default_condition
    ADD CONSTRAINT "FK_258c8610b040dbc83d70c78d45d" FOREIGN KEY (area_key_id) REFERENCES public.area_key(id);


--
-- Name: order_condition FK_291e45ea39145b91b4df71a356b; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.order_condition
    ADD CONSTRAINT "FK_291e45ea39145b91b4df71a356b" FOREIGN KEY (order_id) REFERENCES public."order"(id);


--
-- Name: webhook FK_2c6db22158de8965bed31709357; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.webhook
    ADD CONSTRAINT "FK_2c6db22158de8965bed31709357" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: providers_areas FK_2df7797611bb9500c8101fe637b; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.providers_areas
    ADD CONSTRAINT "FK_2df7797611bb9500c8101fe637b" FOREIGN KEY (area_id) REFERENCES public.area(id);


--
-- Name: lead FK_3c2de62e32b44a485c9e4f6b16f; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead
    ADD CONSTRAINT "FK_3c2de62e32b44a485c9e4f6b16f" FOREIGN KEY (zip_id) REFERENCES public.zip(id);


--
-- Name: payment FK_3e9244ca0c1af4a3601f1795ff6; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT "FK_3e9244ca0c1af4a3601f1795ff6" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: client_default_condition FK_44df5dc05c38c20bd8fadfd9c04; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client_default_condition
    ADD CONSTRAINT "FK_44df5dc05c38c20bd8fadfd9c04" FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: providers_areas FK_4f983c813f430153f00e6417503; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.providers_areas
    ADD CONSTRAINT "FK_4f983c813f430153f00e6417503" FOREIGN KEY (provider_id) REFERENCES public.provider(id);


--
-- Name: clients_areas FK_50f39fb1c5bda549fe5e20e6db7; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.clients_areas
    ADD CONSTRAINT "FK_50f39fb1c5bda549fe5e20e6db7" FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: notification FK_519997b6af92b3a98a52d03d3d6; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT "FK_519997b6af92b3a98a52d03d3d6" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: lead FK_52de4a7df6dbaed9805b5e3971e; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead
    ADD CONSTRAINT "FK_52de4a7df6dbaed9805b5e3971e" FOREIGN KEY (area_id) REFERENCES public.area(id);


--
-- Name: area_key FK_54672c92f88908fa571776e9943; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_key
    ADD CONSTRAINT "FK_54672c92f88908fa571776e9943" FOREIGN KEY (area_id) REFERENCES public.area(id);


--
-- Name: provider FK_55278da99e4170c541b9b73c0c7; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.provider
    ADD CONSTRAINT "FK_55278da99e4170c541b9b73c0c7" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: orders_leads FK_5f8890fc902d229517c6310f538; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_leads
    ADD CONSTRAINT "FK_5f8890fc902d229517c6310f538" FOREIGN KEY (lead_id) REFERENCES public.lead(id);


--
-- Name: sitekey FK_69a1817c5c08d7b547d994b2029; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.sitekey
    ADD CONSTRAINT "FK_69a1817c5c08d7b547d994b2029" FOREIGN KEY (provider_id) REFERENCES public.provider(id);


--
-- Name: client FK_75fffe092dc8bb594720f9b7598; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT "FK_75fffe092dc8bb594720f9b7598" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: leads_properties FK_7925961938f10296490a786e1a1; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.leads_properties
    ADD CONSTRAINT "FK_7925961938f10296490a786e1a1" FOREIGN KEY (area_key_id) REFERENCES public.area_key(id);


--
-- Name: transaction FK_87d332611ebc2beababe8dc4d18; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT "FK_87d332611ebc2beababe8dc4d18" FOREIGN KEY (payment_id) REFERENCES public.payment(id);


--
-- Name: orders_zips FK_8a874c3b3f30da21962ee861842; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_zips
    ADD CONSTRAINT "FK_8a874c3b3f30da21962ee861842" FOREIGN KEY (order_id) REFERENCES public."order"(id);


--
-- Name: client FK_8cf18ed1dd92dbb0393fe78fdfe; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT "FK_8cf18ed1dd92dbb0393fe78fdfe" FOREIGN KEY (webhook_id) REFERENCES public.webhook(id);


--
-- Name: leads_properties FK_952d5aa43709853b3e420af3df9; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.leads_properties
    ADD CONSTRAINT "FK_952d5aa43709853b3e420af3df9" FOREIGN KEY (lead_id) REFERENCES public.lead(id);


--
-- Name: order FK_9b20a91a3c159c1805fc34c08af; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "FK_9b20a91a3c159c1805fc34c08af" FOREIGN KEY (client_area_id) REFERENCES public.clients_areas(id);


--
-- Name: user FK_9e70b5f9d7095018e86970c7874; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_9e70b5f9d7095018e86970c7874" FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: invoice FK_a32ef5f07283d881032b4fa961e; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT "FK_a32ef5f07283d881032b4fa961e" FOREIGN KEY (transaction_id) REFERENCES public.transaction(id);


--
-- Name: lead FK_b3d6afa328fcd97b355a6b0ca33; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.lead
    ADD CONSTRAINT "FK_b3d6afa328fcd97b355a6b0ca33" FOREIGN KEY (sitekey_id) REFERENCES public.sitekey(id);


--
-- Name: invoice FK_b63cfba23ecc43531b5c571ffa3; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT "FK_b63cfba23ecc43531b5c571ffa3" FOREIGN KEY (payment_id) REFERENCES public.payment(id);


--
-- Name: order_condition FK_eb792b6340d5fcdd7ee4b6896b9; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.order_condition
    ADD CONSTRAINT "FK_eb792b6340d5fcdd7ee4b6896b9" FOREIGN KEY (area_key_id) REFERENCES public.area_key(id);


--
-- Name: ws_logs FK_ebd6db106b07ca228e50b66ab45; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.ws_logs
    ADD CONSTRAINT "FK_ebd6db106b07ca228e50b66ab45" FOREIGN KEY (lead_id) REFERENCES public.lead(id);


--
-- Name: clients_areas FK_edf272b467426ea6ada95b427be; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.clients_areas
    ADD CONSTRAINT "FK_edf272b467426ea6ada95b427be" FOREIGN KEY (area_id) REFERENCES public.area(id);


--
-- Name: orders_leads FK_f16962f09b43ad74af1fe0c5156; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_leads
    ADD CONSTRAINT "FK_f16962f09b43ad74af1fe0c5156" FOREIGN KEY (invoice_id) REFERENCES public.invoice(id);


--
-- Name: area_default_condition FK_f17d8c39e70f707a6c3937e8242; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.area_default_condition
    ADD CONSTRAINT "FK_f17d8c39e70f707a6c3937e8242" FOREIGN KEY (area_key_id) REFERENCES public.area_key(id);


--
-- Name: transaction FK_f64616bd3e93b6eb8b7d1e1ff1b; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT "FK_f64616bd3e93b6eb8b7d1e1ff1b" FOREIGN KEY (lead_id) REFERENCES public.lead(id);


--
-- Name: orders_leads FK_f663121ce0dfded41d23608c014; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public.orders_leads
    ADD CONSTRAINT "FK_f663121ce0dfded41d23608c014" FOREIGN KEY (order_id) REFERENCES public."order"(id);


--
-- Name: order FK_f9984bccb61a232d4256360ee95; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "FK_f9984bccb61a232d4256360ee95" FOREIGN KEY (webhook_id) REFERENCES public.webhook(id);


--
-- Name: user FK_fb2e442d14add3cefbdf33c4561; Type: FK CONSTRAINT; Schema: public; Owner: exploit
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_fb2e442d14add3cefbdf33c4561" FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- PostgreSQL database dump complete
--

