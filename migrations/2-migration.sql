
START TRANSACTION;


alter table clients_areas drop column check_curl;
alter table clients_areas drop column lead_curl;
alter table lead drop column buying_price;

DELETE FROM ws_logs
WHERE CAST(FROM_UNIXTIME(ws_logs.created_at / 1000) as date) < now() - interval 30 DAY;

drop table knex_migrations;
drop table knex_migrations_lock;

UPDATE address
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE address CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE address CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE address
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE area
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE area CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE area CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE area
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE area_default_condition
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE area_default_condition CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE area_default_condition CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE area_default_condition
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE area_key
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE area_key CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE area_key CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE area_key
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE client
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE client CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE client CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE client
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE client_default_condition
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE client_default_condition CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE client_default_condition CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE client_default_condition
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE clients_areas
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE clients_areas CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE clients_areas CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE clients_areas
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE company
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE company CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE company CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE company
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
UPDATE company
	SET name = CONCAT(name, '_', id)
	WHERE UPPER(name) IN
	(SELECT UPPER(name) FROM company GROUP BY UPPER(name) HAVING COUNT(*) > 1);

UPDATE invoice
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE invoice CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE invoice CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE invoice
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE lead
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE lead CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE lead CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE lead
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE lead_external_ref
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE lead_external_ref CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE lead_external_ref CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE lead_external_ref
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE lead_rejected
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE lead_rejected CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE lead_rejected CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE lead_rejected
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE leads_properties
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE leads_properties CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE leads_properties CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE leads_properties
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE notification
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE notification CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE notification CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE notification
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE `order`
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000,
		start_at = start_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
UPDATE `order`
	SET end_at = end_at / 1000
	WHERE end_at IS NOT NULL;
ALTER TABLE `order` CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE `order` CHANGE COLUMN updated_at updated_at varchar(255);
ALTER TABLE `order` CHANGE COLUMN start_at start_at varchar(255);
ALTER TABLE `order` CHANGE COLUMN end_at end_at varchar(255);
UPDATE `order`
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime),
		start_at = CAST(FROM_UNIXTIME(start_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
UPDATE `order`
	SET end_at = CAST(FROM_UNIXTIME(end_at) as datetime)
	WHERE end_at IS NOT NULL;
UPDATE `order`
	SET capping_max = -1
	WHERE capping_max IS NULL;

UPDATE order_condition
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE order_condition CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE order_condition CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE order_condition
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
UPDATE order_condition
	SET created_at = NOW(),
		updated_at = NOW()
	WHERE created_at IS NULL OR updated_at IS NULL;

UPDATE orders_leads
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE orders_leads CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE orders_leads CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE orders_leads
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE orders_zips
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE orders_zips CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE orders_zips CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE orders_zips
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE payment
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE payment CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE payment CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE payment
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE provider
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE provider CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE provider CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE provider
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE providers_areas
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE providers_areas CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE providers_areas CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE providers_areas
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE role
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE role CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE role CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE role
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE sitekey
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE sitekey CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE sitekey CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE sitekey
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE transaction
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE transaction CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE transaction CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE transaction
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE user
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE user CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE user CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE user
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE webhook
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE webhook CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE webhook CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE webhook
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE ws_logs
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE ws_logs CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE ws_logs CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE ws_logs
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

UPDATE zip
	SET created_at = created_at / 1000,
		updated_at = updated_at / 1000
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;
ALTER TABLE zip CHANGE COLUMN created_at created_at varchar(255);
ALTER TABLE zip CHANGE COLUMN updated_at updated_at varchar(255);
UPDATE zip
	SET created_at = CAST(FROM_UNIXTIME(created_at) as datetime),
		updated_at = CAST(FROM_UNIXTIME(updated_at) as datetime)
	WHERE created_at IS NOT NULL AND updated_at IS NOT NULL;

COMMIT;